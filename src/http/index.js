import axios from 'axios'
import {Message} from 'element-ui'
import store from '../store'
import router from '../router'

const instance = axios.create({
    baseURL:'http://192.168.1.179:8888/api/private/v1'
})

//登录状态请求拦截器 //还未发送axios
instance.interceptors.request.use((config)=>{ 
    if(store.state.token){
        config.headers['Authorization'] = store.state.token
    }
    return config
},(error)=>{
    return Promise.reject(error)
})

//响应拦截器  //已经发送axios
instance.interceptors.response.use((response)=>{
    // console.log(response.data.meta.status)
    // if(response.data.meta.status === '400'){
    //     router.push({
    //         name:'login'
    //     })
    // }
    if(response.data.meta.msg === '无效token'){
        router.push({
            name:'login'
        })
    }
    return response
},(error)=>{
    return Promise.reject(error)
})



export function http( url , method , data , params){
    return new Promise((resolve,reject)=>{
        instance({
            url,
            method,
            data,
            params
        })
        .then(res=>{ //请求回来数据
            if((res.status >= 200 && res.status < 300) || res.status === 304){
                if((res.data.meta.status >= 200 && res.data.meta.status < 300) || res.data.meta.status === 304){
                    resolve(res.data)
                    // console.log(res)
                }else{
                    Message({
                        showClose:true,
                        message:res.data.meta.msg,
                        type:'error'
                    })
                    // console.log(res) //value空或错误 参数错误
                    reject(res)
                }
            }else{
                Message({
                    showClose:true,
                    message:res.statusText,
                    type:'error'
                })
                // console.log(res)
                reject(res)
            }
        })
        .catch(err=>{  //数据没有请求回来
            Message({
                showClose:true,
                message:err.message,
                type:'error'
            })
            // console.log(res)
            reject(err)
        })
    })
}
