import {http} from './index'


//login
    //登录方法
    export function login(data){
        return  http('login','POST',data)
    }


//index
    //获取权限列表(首页左侧列表)
    export function getMenus(){
        return http('menus','GET')
    }

//users
    //获取用户列表
    export function getUsers(params){
        return http('users', 'GET', {} , params)
    }

    //添加用户
    export function addUsers(data){
        return http('users','POST',data)
    }
    //改变用户状态
    export function changingState(data){
        return http(`users/${data.id}/state/${data.mg_state}`,'PUT',data)
    }

    //点击删除用户
    export function getUsersEdit(id){
        return http(`users/${id}`,'DELETE')
    }

    //点击编辑用户提交
    export function getUsersEditSubmission(id,data){
        return http(`users/${id}`,'PUT',data)
    }

    //请求用户管理列表
    export function getUserRoleList(){
        return http('roles','GET')
    }

    //请求用户信息
    export function getUserInformation(id){
        return http(`users/${id}`,'GET')
    }

    //点击更改用户角色
    export function getChangeUserRole(id,rid){
        return http(`users/${id}/role`,'PUT',{rid})
    }


//roles
    //删除权限
    export function getDeleteRoleJurisdiction(id){
        return http(`roles/${id.roleId}/rights/${id.rightId}`,'DELETE')
    }
    //获取权限列表
    export function getPermissionList(){
        return http(`rights/tree`,'GET')
    }
    //编辑角色授权
    export function getEditSubmitRole(roleId,rids){
        return http(`roles/${roleId}/rights`,'POST',{rids})
    }
    //添加角色
    export function addRoleInformation(data){
        return http('roles','POST',data)
    }
    //删除角色
    export function getDeleteRoles(id){
        return http(`roles/${id}`,'DELETE')
    }
    //编辑角色
    export function getEditorialRole(id,data){
        return http(`roles/${id}`,'PUT',data)
    }

//rights
    //所有权限列表
    export function getpermissionList(){
        return http(`rights/list`,'GET')
    }