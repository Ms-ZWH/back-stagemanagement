import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)

const routes = [
  {
    path:'/',
    redirect:'/Login'
  },
  {
    path: '/Login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "about" */ '../views/Login.vue')
  },
  {
    path:'/index',
    name:'index',
    component: () => import(/* webpackChunkName: "about" */ '../views/index.vue'),
    children:[
      {
        path:'users',
        name:'users',
        component: () => import(/* webpackChunkName: "users" */ '../views/users/Users.vue'),
      },
      {
        path:'roles',
        name:'roles',
        component: () => import(/* webpackChunkName: "roles" */ '../views/roles/Roles.vue'),
      },
      {
        path:'rights',
        name:'rights',
        component: () => import(/* webpackChunkName: "rights" */ '../views/rights/Rights.vue'),
      }
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router
