import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

function initialState(){
  return{
    token:localStorage.getItem('token') || '',//获取 已经储存了的数据
    username:localStorage.getItem('username') || ''
  }  
}

export default new Vuex.Store({
  state: initialState(),
  mutations: {
    setToken(state,data){
      state.token=data
      localStorage.setItem('token',data)//储存 token创建的属性名  data属性名
    },
    setUsername(state,data){
      state.username=data
      localStorage.setItem('username',data)
    },
    resetState(state){
      Object.assign(state,initialState())
    }
  },
  actions: {
  },
  modules: {
  }
})
